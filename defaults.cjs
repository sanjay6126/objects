const { default: testObject } = require("./testObject.cjs");

function defaults(obj,defaultProps){
    if(typeof(obj)!='object' || obj == undefined || obj.constructor != Object){
        return {};
    }
    else if(defaultProps == undefined || defaultProps.constructor != Object || typeof(defaultProps)!='object'){
        return obj;
    }
    else{
        for(index in defaultProps){
            if(obj[index] == undefined){
                obj[index] = defaultProps[index];
            }
        }
        return obj;
    }
    
}


module.exports = defaults;