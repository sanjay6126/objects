function invert(obj){
    var result = {};

    if(typeof(obj)!='object' || obj == undefined || obj.constructor != Object){
        return {};
    }
    else{
        for(var key in obj){
            result[obj[key]] = key;
        }
    }

    
    return result;
      
    
}


module.exports = invert;