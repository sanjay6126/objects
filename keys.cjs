function keys(obj){

    if(typeof(obj)!='object' || obj == undefined || obj.constructor != Object){
        return {};
    }
    
    var res = [];
    for (const key in obj) {

        res.push(`${key}`);
    }
    return res;

    
}



module.exports = keys;