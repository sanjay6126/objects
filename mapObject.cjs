

function mapObject(obj, cb) {
    if (obj.length == 0 || typeof (obj) != 'object') {
        return obj = {};
    }
    if(typeof(cb)!='function')
    {
        return cb={};
    }


    let result = {};
    for (let key in obj) {
        if (cb(obj[key])) {
            result[key] = cb(obj[key], key);
        }

    }
    return result;

}




module.exports = mapObject;