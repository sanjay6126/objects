function values(obj){

    if(typeof(obj)!='object' || obj == undefined || obj.constructor != Object){
        return {};
    }
    else{
        var res = [];
        for (const key in obj) {

            res.push(`${obj[key]}`);
        }
        return res;

    }
}

module.exports = values;